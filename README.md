# shaaaare-app

The application files structure follows the standard Vue application layout.
Everything relevant to the program logic is stored under './src'


## A test application is deployed here:
Test Application under: https://zellmer.gitlab.io/CBS-IPAD-Project/
Test user: test@test.test
Test user password: test1234


Or install locally:

## First: Project setup
```
npm install
```

## Second: Run programm
### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
