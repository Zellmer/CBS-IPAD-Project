import firebase from 'firebase'
import 'firebase/firestore'

// Initialize Firebase
var config = {
    apiKey: "AIzaSyALHrl1JeLRrmfQAIBZl94oNejF5AWHqJc",
    authDomain: "cbs-ipad.firebaseapp.com",
    databaseURL: "https://cbs-ipad.firebaseio.com",
    projectId: "cbs-ipad",
    storageBucket: "cbs-ipad.appspot.com",
    messagingSenderId: "1085661060132"
};
firebase.initializeApp(config);

// Define short handles for regularly used objects
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser
const storage = firebase.storage()

// date issue fix according to  documentation
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

// firebase collection for users
const usersCollection = db.collection('users')

//make fb config accessible
export {
    db,
    auth,
    currentUser,
    usersCollection,
    storage
}