import Vue from 'vue'
import Vuex from 'vuex'
const fb = require('./firebaseConfig.js')

Vue.use(Vuex)


// handle page reload and store data if user is authenticated
fb.auth.onAuthStateChanged(user => {
  if (user) {
    store.commit('setCurrentUser', user)
    store.dispatch('fetchUserProfile')
    store.dispatch('fetchUserCourses')

    fb.usersCollection.doc(user.uid).onSnapshot(doc => {
      store.commit('setUserProfile', doc.data())
    })
  }
})


export const store = new Vuex.Store({
  // define the empty stoe state
  state: {
    currentUser: null,
    userProfile: [],
    courses: []
  },

  actions: {
    // clear the current user from the local state - currently not used, only for development
    clearData({ commit }) {
      commit('setCurrentUser', null)
    },

    // Get the current user profile from firebase
    fetchUserProfile({ commit, state }) {
      fb.usersCollection.doc(state.currentUser.uid).get().then(res => {
        commit('setUserProfile', res.data())
      }).catch(err => {
        console.log(err)
      })
    },

    // Get the current user courses collection form firebase
    fetchUserCourses({ commit, state }) {
      const coursesCollection = fb.usersCollection.doc(state.currentUser.uid).collection('courses')
      let coursesCollectionTemp = [];

      // Load all courses
      coursesCollection.orderBy('name').get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {

          // Iterate over courses and get all sessions
          const sessionsCollection = coursesCollection.doc(doc.id).collection('sessions')
          let courseData = doc.data();
          Vue.set(courseData, 'sessions', []);
          Vue.set(courseData, 'id', doc.id);

          sessionsCollection.get().then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {

              // Iterate over sessions and get all assets
              const assetCollection = sessionsCollection.doc(doc.id).collection('assets')
              let sessionData = doc.data();
              Vue.set(sessionData, 'assets', []);
              Vue.set(sessionData, 'id', doc.id);

              assetCollection.get().then(function(querySnapshot) {
                querySnapshot.forEach(function(doc) {

                  // Iterate over assets and store all to array
                  let assetData = doc.data();
                  Vue.set(assetData, 'id', doc.id);
                  sessionData.assets.push(assetData)

                })
              })
              courseData.sessions.push(sessionData)
            })

            // Order all sessions by date
            courseData.sessions.sort(function(a,b){
              // Turn your strings into dates, and then subtract them
              // to get a value that is either negative, positive, or zero.
              return new Date(a.date) - new Date(b.date);
            });
          })
          coursesCollectionTemp.push(courseData)
        });
        commit('setUserCourses', coursesCollectionTemp)
      })

      .catch(function(error) {
        console.log("Error getting documents: ", error);
      });
    }
  },
  mutations: {
    setCurrentUser(state, val) {
      state.currentUser = val
    },
    setUserProfile(state, val) {
      state.userProfile = val
    },
    setUserCourses(state, val) {
      state.courses = val
    }
  }
})