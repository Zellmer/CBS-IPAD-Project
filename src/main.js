import Vue from 'vue'
import App from './App.vue'
import router from './router'
import {store} from './store'

// import the firebase configutation
const fb = require('./firebaseConfig.js');
import './assets/sass/app.sass'
import VueMoment from 'vue-moment'

Vue.config.productionTip = false;

// Make moment.js accessible throughout the application
Vue.use(VueMoment);

let app;

// handle page reloads and rerender application
fb.auth.onAuthStateChanged(user => {
    if (!app) {
        app = new Vue({
            el: '#app',
            router,
            store,
            render: h => h(App)
        })
    }
});
