import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login'
import Dashboard from './components/Dashboard'
import Signup from './components/Signup'
import Readings from './components/Readings'
import Home from './views/Home'
import Application from './views/Application'
import firebase from 'firebase'

Vue.use(Router);


// Define all routes for the application
const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children: [
        {
        path: '/login',
        component: Login
        },
        {
        path: '/signup',
        component: Signup
      }]
    },
    {
      path: '/application',
      name: 'application',
      component: Application,
      meta: {
          requiresAuth: true
      },
      children: [
        {
          path: '/dashboard',
          component: Dashboard,
          meta: {
            requiresAuth: true
          }
        },
        {
          path: '/readings',
          component: Readings,
          meta: {
            requiresAuth: true
          }
        }]
    }
  ]
});

// Handle authentication on requesting access to route
router.beforeEach((to, from, next) => {
    let requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const currentUser = firebase.auth().currentUser

    if (requiresAuth && !currentUser) {
        next('/')
    } else if (requiresAuth && currentUser) {
        next()
    } else {
        next()
    }
});


export default router;